import os
from os import walk
from os import listdir
from os.path import isfile

#removes all parts of the files that are not between the indicated tags (balises)

#infilename: name of the file to be parsed
#outfilename; name of the output file
#balisenames: list of the tag names that should be kept ['HEADER', 'TEXT', ...]
#lonebalisenames: list of the tag names that do not directly contain text and should be kept ['DOC']

#assumes the tags are correct in the file
def conv(infilename,outfilename, lonebalisenames, contentbalisenames, encode):
	inf = open(infilename,'r', encoding = encode)
	outf = open(outfilename,'w', encoding = encode)

	keeping = False
	opencount = 0

	for line in inf.readlines():
		if(keeping):
			outf.write('\n')
		for word in line.split():
			if word[0]=="<": #balise
				#voir si elle est ouvrante ou fermante
				crtbalise = ""
				opening = False
				if(len(word)<2):
					print("File abandoned: "+infilename)
					break
				if(word[1]=="/"):
					crtbalise = word[2:(len(word)-1)]
				else:
					crtbalise = word[1:(len(word)-1)]
					opening = True
				if(crtbalise in contentbalisenames):
					outf.write(word+'\n')
					if(opening):
						opencount = opencount + 1
					else:
						opencount = opencount - 1
					keeping = opencount > 0
				elif(crtbalise in lonebalisenames):
					outf.write(word+'\n')
			elif(keeping):
				outf.write(word+' ')


	inf.close()
	outf.close()

def ensure_dir(file_path):
	if not os.path.exists(file_path):
		os.makedirs(file_path)


def convall(indirectoryname, infilesformat, outdirectoryname, lonebalisenames, contentbalisenames, encoding):

	files = []
	for (dirpath, dirnames, filenames) in walk(indirectoryname):
		files.extend(filenames)
		break

	files = [f for f in files if infilesformat in f] # get rid of readme, etc.
	ensure_dir(outdirectoryname)

	for f in files:
		outf = outdirectoryname +'/'+ f
		inf = indirectoryname +'/'+ f
		conv(inf,outf, lonebalisenames, contentbalisenames, encoding)

