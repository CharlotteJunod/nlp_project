%\documentclass{article}
%\documentclass[journal]{IEEEtran}
%\documentclass{report}

\documentclass[12pt]{article}
\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{listings}
\usepackage{float}
\usepackage{multicol}


\begin{document}
\pagenumbering{gobble}

\begin{titlepage}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for the horizontal lines, change thickness here

\center % Center everything on the page
 
%----------------------------------------------------------------------------------------
%	HEADING SECTIONS
%----------------------------------------------------------------------------------------

\textsc{\LARGE University of Neuchâtel}\\[1.5cm] % Name of your university/college
\textsc{\Large NLP Seminar Project}\\[0.5cm] % Major heading such as course name
\textsc{\large Report}\\[0.5cm] % Minor heading such as course title

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\HRule \\[0.4cm]
{ \huge \bfseries Language Identification}\\[0.4cm] % Title of your document
\HRule \\[1.5cm]
 
%----------------------------------------------------------------------------------------
%	AUTHOR SECTION
%----------------------------------------------------------------------------------------

\begin{minipage}{0.4\textwidth}
\begin{flushleft} \large
\emph{Author:}\\
Charlotte \textsc{Junod} % Your name
\end{flushleft}
\end{minipage}
~
\begin{minipage}{0.4\textwidth}
\begin{flushright} \large
\emph{Supervisor:} \\
Prof. Jacques \textsc{Savoy} % Supervisor's Name
\end{flushright}
\end{minipage}\\[2cm]

% If you don't want a supervisor, uncomment the two lines below and remove the section above
%\Large \emph{Author:}\\
%John \textsc{Smith}\\[3cm] % Your name

%----------------------------------------------------------------------------------------
%	DATE SECTION
%----------------------------------------------------------------------------------------

{\large 21.12.2018}\\[2cm] % Date, change the \today to a set date if you want to be precise

%----------------------------------------------------------------------------------------
%	LOGO SECTION
%----------------------------------------------------------------------------------------
 
%----------------------------------------------------------------------------------------

\vfill % Fill the rest of the page with whitespace

\end{titlepage}

\pagebreak
\begin{abstract}
This report is related to a project made for a seminar course in the University of Neuchâtel. The goal of the project is to explore and develop a solution to recognize in which language a text was written, given a small number of possible languages. More precisely, we want to be able to distinguish close languages effectively. The solutions use machine learning algorithms to classify the languages. The best solution is based on counting the most frequent words and representing it as a vector, then comparing vectors using a distance metric. The accuracy of this solution is over 95\%.
%AJOUTER RESULTATS
\end{abstract}

\pagebreak
%ADD TABLE DES MATIERES
\tableofcontents
\pagebreak

%slides du cours:
%Question / state of the art / your solution / your analysis
\pagenumbering{arabic}
\setcounter{page}{1}
\section{Introduction}
This report gives details on a project that was made in the context of a seminar course in the University of Neuchâtel. The seminar's subject is Natural Language Processing (NLP). The main topic of the project is recognizing the language in which a text was written. Indeed, nowadays this kind of application becomes more and more useful; a lot of web pages are published, often without any information on their language in their metadata. It is important for search engines or other sorting applications to know in which language a document was written in order to provide good services.

More precisely, this project focuses on one challenge of such applications: recognizing two languages that are close to each other, such as Spanish and Italian for example. Indeed, as we would expect, this is more difficult than recognizing more different languages, such as English and Chinese for example - more difficult meaning that with the same kind of algorithm there is more confusion between the two languages.
On top of that, the project will focus on recognizing the language using short amounts of text.
The data that was used to develop the solution is made of journal articles in Portuguese, Spanish and English. Spanish and Portuguese are two close languages. The third language gives a reference case as a language that should be easier to distinguish from the other two.

The NLP issue presented here has been explored already and various machine learning based solutions have been developed to resolve it. The next chapter, \textbf{State of the Art}, gives a view on the current solutions, their strengths and weaknesses. The chapter after, \textbf{Methodology}, gives details on the provided corpus, on the models that have been considered for the application and on how the evaluation was planned. The evaluation itself is given in the chapter \textbf{Evaluation}. Here are the results of the developed models and a comparison with existing solutions. Finally, the chapter \textbf{Conclusion} summarizes the subject and gives a synthesis of the project. The \textbf{References} are also available at the end of the report; in the report, references are indicated by a number between $[$ $]$. Finally an \textbf{Annex} contains the complete results of the classifiers.

\pagebreak
\section{State of the Art}
Nowadays, with the growth of the internet and the multiplication of web pages, being able to sort and classify big numbers of documents is necessary. Especially, browsers and research browsers need to sort those pages based on the language they were written in, in order to provide relevant results and insure good services. It is also important for other kinds of application, such as social media applications or translators that will automatically detect the source language, or automated solutions that are building corpora using web pages.

Language recognition is one of the numerous applications of NLP. This field of computer science counts several common approaches to decompose and analyse text written in natural language; a number of them are commonly used for language recognition, with different results according to the goal of the application.

A common approach nowadays is to rely on words n-grams to decompose and analyse a text. N-grams are groups of n characters that we retrieve from a text; for example, if we take 3-grams from the word "tree", we have " tr", "tre", "ree" and "re " (as we count spaces too). An analysis is made based on the frequency of those n-grams in the training set. This method works well for classifiers that are made to recognize as much different languages as possible. A famous example that uses n-grams would be langid.py $[2]$. This approach is widely used and usually classifiers obtain over 95\% accuracy $[1]$, $[2]$. However, with this method, languages that are not close to each other (for example English and Italian) are easier to classify than close languages (for example Italian and Spanish), which would probably have a lower accuracy than the overall results. 

% ADD MFW
Another approach is to take not n-grams but the words in the text as primary elements. Several models base themselves on which words are present and with which frequency.

More customized solutions also exist; some models focus on a few particular languages and the algorithm can be optimized for those languages. For example, some models use a list of forbidden words or numbers format to identify texts $[1]$. Those can focus on close languages and be tuned to have better results than more generic solutions.

\pagebreak
\section{Methodology}
This chapter explains in details how the project was planned and developed.
The overall methodology is based on common working methods used in machine learning projects. The corpus was divided into a training set and a test set. The training set, made of 80\% of the data, was used to build the models. The test set was kept apart and used to evaluate the models and compute common statistics based on the confusion matrix. One important statistic was accuracy, as it is the statistic used in most papers to present a model's performance. This way we can compare our models to others and get a hint about its actual performance.

The section \textbf{Corpus} details what kind of data was used and the section \textbf{Models} explains the models that were used for this project, as well as other models that may be interesting for our application.

\subsection{Corpus}
The provided corpus is a set of journal articles in English, Spanish and Portuguese. The data is given in several text files, sorted in folders by language. Each file contains several articles, separated by xml tags. Depending on the language, there is additional data provided; only the headers and the text of each article were kept to train and evaluate the models.

The data is unbalanced, in particular there is much more text in Spanish than in the other languages, as shown in the table below.\\
\begin{figure}[H]
\begin{center}
\begin{tabular}{|l|l|l|l|}
\hline
	Language & nb of articles & Total word count & \% of total \\
	\hline
   English & 10163 & 3654113 & 5.02\\
   Portuguese & 51875 & 13762781 & 18.92 \\
   Spanish & 215738 & 55319498 & 76.05 \\
   \hline
\end{tabular}\\
  \caption{Proportion of text in each language in the training set}
  \end{center}
\end{figure}

This observation will be important when building the models. It is necessary to know if having unbalanced training data can give a bias to the model, and if so, make sure that the data is balanced before using it. Also, when testing the model the test data needs to be evenly distributed to have fair statistics.

The selected approach for this project is based on analysing words frequency. For this approach, punctuation is not needed; it was therefore removed from the text. Also the frequency statistics for each language are based on the whole training data; there was no need to keep the separation between articles.\\

For the test set, the data had to be balanced. Indeed, having much more data of one type (in our case, Spanish) than of the other types would heavily influence the statistics. To balance the test set, Spanish and Portuguese documents were removed in order to have the same amount of data for each language. The data was also treated as a whole rather than separated by article; this way it was possible to vary the number of words in the testing samples.

\subsection{Models}
For this project, the suggested approach was to retrieve features from the training data and to represent them as a vector. From this, several models can be built. A first idea was to take n-grams or most frequent words for each language and represent them as a vector of apparition probability, then use the Kullback-Leibler Divergence (or KLD) to compute the divergence between a sample and the reference vectors. The divergence is computed as follows:
\begin{center}
$D_{KL}(P||Q) = \sum_i {P(i)*log(P(i)/Q(i))}$\\
\end{center}
P and Q being two vectors of probabilities of the same length, i being the index in the vectors. The KLD computes the divergence between two probability distributions.\\

To use KLD, two common statistical approaches are possible: using n-grams or using words frequency. However n-grams are often used with other kinds of models - Bayes classifiers or Hidden Markov Models especially - while the most common words are used with models more similar to the KLD's approach, such as centroid-based models or models using Zipf's law $[$1$]$.\\

The approach that was implemented is based on most frequent words. Depending on the language in which a text is written, the most frequent words will be different. We can take advantage of this and use it as our features vector, compute the probability of apparition of each word and apply the KLD on it. Another model was also implemented, this second model relies on a centroid-based approach.

\subsubsection{Data representation}
The implemented models rely on the same representation of the data. This representation consists in building three vectors of probabilities, one for each language. These vectors will be called "frequent words vectors" in this report. Each dimension in the vector represents one word that appears in the corpus; the associated value is the probability for this word to appear in the vector's language. The following table shows a part of the corpus' English vector.\\
\begin{figure}[H]
\begin{center}
\begin{tabular}{|l|l|l|l|l|}
	\hline
   the & of & to & ... & nice\\
   \hline
   4724& 2485 & 2344 & ... & 1 \\
   \hline
\end{tabular}\\
  \caption{Part of the English most frequent words vector}
  \end{center}
\end{figure}

To build the language vectors, first the number of words in the training set is counted for each language separately. Then the files are parsed again and, for each language, a dictionary is built having the words in the articles as keys and the number of times the words have been seen as value. 

Now, each of the three dictionaries is changed into a vector and extended with the missing words from the other languages. Laplace smoothing was used to avoid probabilities of 0; the value of a feature is the number of apparitions of the word in the language's corpus + 1, or 1 if it does not appear.

Then, for each of the three dictionaries, the values will be divided by the total number of words for the corresponding language (plus the total number of different words, to respect Laplace's smoothing) to obtain the probability that a word from an article in the corresponding language is the same as the key - according to the corpus. This computation is made when classifying the samples.

To classify a sample, we need to construct its most frequent words vector; we look for every word corresponding to a feature in the existing vectors, count its occurrences in the sample text, and set the corresponding sample's feature to this value. Note that new words - that appear in the sample but not in the training set - are not taken into account here.

When doing the classification, the actual probability of apparition of a word must still be computed. For a reference language vector, each feature is divided by the total number of words taken into account. For a sample, the feature values are divided by the total number of words in the sample.

With this solution, the training set does not need to be balanced, as having more data for one language will make its vector more accurate, but will not harm nor overwhelm the other vectors' result.

\subsubsection{Classification methods}
The first implemented solution is based on the KLD. To classify a sample, we compute its frequent words vector and compare it to the three languages' vectors. The probabilities corresponding to each feature are computed and then compared by using D$_{kl}$ $(sample || language)$ (the divergence between the sample and the language). The sample is classified as the language that gives the smallest divergence in absolute value.

A second solution uses the exact same data representation as the KLD model, but uses a distance metric - the Manhattan distance - instead of KLD to determine the closest language. In this case,the probability of apparition for each word is also used instead of just the number of apparitions; it acts as a normalization of the data. This approach can be represented as a centroid-based model; the centroid of each language's space is computed as its frequent words vector and then a sample is classified based on which centroid it is the closest to.

These models can be tuned by modifying the data that is given to them; first, all the words in the training set were used to build the vectors and compare them, then these vectors were reduced and used only the 50 most frequent words of each language. As illustrated in the next chapter, \textbf{Evaluation}, this solution gives better results.

It would be possible to use other models to resolve the current problem. To develop further the distance model, a KNN could be implemented. Every article would be used as a point in the model. The problem here is that there would be a big number of points, probably too big compared to how the model would work with a reduced number of points. To avoid this problem and still use all the training data, we would first define the variable n, which is how many points we want to keep for each language. Then all the articles in the training set would be divided into 3*n groups, making n groups in each language (without mixing the languages). Then each point would be computed based on a group of articles and not on a single article. This would allow to use less points and still use all the data. Also, it has the same property as the vector model: we can have unbalanced data in the training set without biasing the model.

%langid.py?
\pagebreak
\section{Evaluation}
This chapter presents the implemented models' performances. For the evaluation, the test set that was created before building the models was used. This test set contains approximately the same amount of words from each language of the corpus.

In this chapter four different models variations will be presented: the KLD model using all words, the KLD model using only the 50 most frequent words of each language, the distance model using all words and the distance model using only the 50 most frequent words of each language. All the implementation was made in Python.

\subsection{KLD}
This model is the one that was implemented first. All the words in the training set are represented in the most frequent words vectors.

This first confusion matrix shows the results of the very first - naive - implemented solution. More detailed results are available in the \textbf{Annex}.
\begin{figure}[H]
\begin{lstlisting}
Using all words
Classification with 23045 samples

Confusion matrix: (pred\real)
	EN	PT	SP	
EN	8402	7496	7147	
PT	0	0	0	
SP	0	0	0	

On average: 
Precision: 0
Accuracy: 0.6898676502495118
Recall: 0.0
F1: 0
\end{lstlisting}
  %\caption{Results of the first distance model}
\end{figure}
We can see that all instances were classified as being in English; this is far from being a viable model. Therefore the data representation was modified to use only the 50 most frequent words. The result of the KLD classification with this new vector is shown below. More detailed results are available in the \textbf{Annex}.

\begin{figure}[H]
\begin{lstlisting}
50 most frequent words
Classification with 23045 samples

Confusion matrix: (pred\real)
	EN	PT	SP	
EN	0	0	0	
PT	8402	7496	7147	
SP	0	0	0	

On average: 
Accuracy: 0.6898676502495118
\end{lstlisting}
  %\caption{Results of the first distance model}
\end{figure}
Here the problem is the same; apparently the way the data is represented combined with the KLD does not make a good classifier. A suspicion is that the parts of the vectors that have 0 word apparitions influence the result a lot, and since between any two vector, there is a common part where the minimal probability appears a lot of times - the part that corresponds to the language(s) that none of the vectors refer to - this may have too much influence on the classification. 
There may be a way to apply KLD on the data that gives actual results, but as far as this project is concerned, the KLD model has then been abandoned and another classification method has been used. This new method is the distance-based model and is presented in the next section.

\subsection{Distance}
This model's results are more interesting. First all words in the corpus were used in the vectors, as for the first KLD model. The samples are classified using the Manhattan distance. The first confusion matrix here under shows the same symptoms as the KLD: all instances are classified as the same language. Considering these results, it seems that the issue is what was suspected. Indeed, with a distance metric the same problem may appear, as irrelevant data will make a sample point closer to a reference point than it should be; vectors that are actually from different languages seem very similar. More detailed results are available in the \textbf{Annex}.
\begin{figure}[H]
\begin{lstlisting}
Classification with 23045 samples

Confusion matrix: (pred\real)
	EN	PT	SP	
EN	8402	7496	7147	
PT	0	0	0	
SP	0	0	0	

On average: 
Accuracy: 0.6898676502495118
\end{lstlisting}
  %\caption{Results of the first distance model}
\end{figure}

Using only a limited number of the most frequent words in the vector can overcome this problem. In a second version of the distance-based model, only the 50 most frequent words from each language were used, forming reference vectors of length 139. The length is shorter than 150 because languages have some frequent words in common and those are only taken into account once. The table below shows the common words among the 50 most frequent of the three languages, compared to the number of words in common among the whole training set:

\begin{center}
\begin{figure}[H]
\begin{tabular}{|l|l|l|}
\hline
   Languages & Common words in 50 most frequent & total common words \\
	\hline
   English and Spanish & a & 25332\\
   \hline
   English and Portuguese & a, as & 21616\\
   \hline
   Spanish and Portuguese & de, que, a, por, se, para, no, como, dos & 68195 \\
   \hline
\end{tabular}
  \caption{Common words among the 50 most frequent}
\end{figure}
\end{center}

Especially Spanish and Portuguese have a fair number of common words. However, if we take the proportion of Spanish words that appear in Portuguese in the whole training set and in the reduced set, the second proportion is smaller - and therefore better:

\begin{center}
\begin{figure}[H]
\begin{tabular}{|l|l|}
\hline
   proportion in whole set & proportion in reduced set \\
	\hline
   0.28 & 0.18\\
   \hline
\end{tabular}
  \caption{Proportion of words that appear in Spanish and in Portuguese}
\end{figure}
\end{center}

The results with this representation were actually interesting and much closer to state-of-the-art applications' results. The table below shows the accuracy obtained with this model and samples of 100 words. More detailed results are available in the \textbf{Annex}.
\begin{center}
\begin{figure}[H]
\begin{tabular}{|l|l|l|l|l|}
	\hline
   words in sample & EN accuracy & PT accuracy & SP accuracy & mean accuracy \\
   \hline
   100 & 0.999 & 0.992 & 0.993 & 0.994 \\
   \hline
\end{tabular}
  \caption{Accuracy of the distance model with 100 words samples}
\end{figure}
\end{center}

We can note that we have an accuracy of 99\% using samples of 100 words in length. Those are good results since 100 words is the length of a short paragraph, typically a short summary.

Also, the model's complexity was reduced by far. With the previous version, the complexity depended on the total number of different words in the training set and on the length of the sample that gets classified (O(nbwords+sample length)). The complexity is linear because the distance can be computed in O(n), n being the number of dimensions of the considered space, and the sample length is also taken into account because it has to be parsed once to get its vector. Now, as we only use the 50 most frequent words of each language, the complexity drops to maximum O(150+sample length) (it can be a bit less because the vector - like in the current case - may be shorter than 150), which corresponds to O(sample length). It is not linear but constant in matter of training set size.\\

More experiments were made varying the samples length to test the limits of this model. the table below shows the accuracy obtained when varying the samples' lengths; more detailed results are available in the \textbf{Annex}.
\begin{center}
\begin{figure}[H]
\begin{tabular}{|l|l|l|l|l|}
	\hline
   words in sample & EN accuracy & PT accuracy & SP accuracy & overall accuracy \\
   \hline
   500 & 0.998 & 0.991 & 0.992 & 0.994 \\
   \hline
   200 & 0.999 & 0.992 & 0.993 & 0.995 \\
   \hline
   100 & 0.998 & 0.991 & 0.992 & 0.994 \\
   \hline
   80 & 0.998 & 0.968 & 0.969 & 0.978 \\
   \hline
   70 & 0.997 & 0.932 & 0.933 & 0.954 \\
   \hline
   60 & 0.983 & 0.887 & 0.893 & 0.924 \\
   \hline
   50 & 0.983 & 0.840 & 0.854 & 0.893 \\
   \hline
   20 & 0.859 & 0.782 & 0.661 & 0.767 \\
   \hline
\end{tabular}\\
  \caption{Accuracy of the distance model depending on sample length}
\end{figure}
\end{center}
The results show that with a sample length of 100 words or more, the classification gives about 99\% accuracy. Even with less words, we can keep quite good results, down to 80 words the accuracy for all languages is still over 95\%. For shorter samples it begins to decrease, as shown in the graph below.

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/accuracy2.png}
  \caption{Accuracy of the distance model depending on sample length}
\end{figure}

Especially when the sample length goes under 50 words, the accuracy drops. This must be due to the fact that we use the 50 most frequent words as a basis for the vectors; with less than that in the sample, it is normal that the generated vector does not correspond to what is expected.\\

Now, this model can be compared to other models presented in literature. The only point that could not be compared is the number of words in the samples used; this information is given for none of the classifiers presented in the papers considered for this project.

\begin{centering}
\begin{figure}[H]
\begin{tabular}{|l|l|l|l|}
	\hline
   Model & Accuracy & Recall & Languages \\
   \hline
	This project & 95-99\% & 93-99\% & English, Spanish, Portuguese \\
   \hline
   n-gram models & 95-99\% & - & English, Spanish\\
   \hline
   langid.py & 91.3\% & - & 67 different\\
   \hline
   centroid-based models & 97.5\% & - & unknown\\
   \hline
   custom model (Ljubesic et al.) & - & 99\% & Croatian, Slovenian, Serbian\\
   \hline
\end{tabular}\\
  \caption{Accuracy of the distance model depending on sample length}
\end{figure}
\end{centering}

%SYNTHESE
As a summary for this chapter, four model variations were implemented. The classification was made by two means: the Kullback-Leibler Divergence and the Manhattan distance. The first did not give any interesting results while the second, used with vectors made of the 50 most frequent words of each language, finally made a good model for this application - it is able to recognize the language of whort texts (short being 80 words or more) with an accuracy over 95\%.

\pagebreak


\section{Conclusion}
%résumé des résultats
This project explored a few possible solutions for classifying texts depending on their language; several models were developed for this application. The most relevant to keep in mind here is the second distance model, which accuracy was comparable with the models presented in papers $[1]$, $[2]$. Even for a model having a very specific goal, as what was developed here, it is possible to obtain good results while keeping the implementation clean and not too specialized - the models could be easily rebuilt to identify other languages than English, Spanish ans Portuguese if they were trained with articles in other languages.

Also this opens the way to further experimentation; the distance model could be modified to become a KNN for example, or it would be possible to change the data representation and use the most frequent words 3-grams instead of the most frequent words for example. Other possibilities could be considered too: using the punctuation specific to one language to refine the models for example.

%résumé du déroulement
As for the project's development process, the first phase that consisted in getting to know the corpus - establishing statistics and cleaning the data - took its fair amount of time. Several tracks that were abandoned very soon and therefore are only evoked here have been considered, in particular a choice had to be made on wether n-grams or words would be used to represent the data. Also using the punctuation and special characters was considered as a mean to identify the language in case the models would need some final refinement.

%feedback perso ?

\pagebreak
\section{References}
%les 2 papers
$[1]$ Zampieri, M. (2016). Automatic Language Identification. Working with Text, 189-208.\\

\noindent
$[2]$ Marco Lui , Timothy Baldwin, langid.py: an off-the-shelf language identification tool, Proceedings of the ACL 2012 System Demonstrations, p.25-30, July 10-10, 2012, Jeju Island, Korea 
%langid.py
\pagebreak
%AJOUTER ANNEXE RESULTATS COMPLETS
\section{Annex}
This section contains the complete results - confusion matrix, precision, accuracy, recall, f1-score - of the classification made on the test set using the different implemented models.\\

\textbf{KLD model 1 (using all words)}
\begin{lstlisting}
Using all words
23045 samples
Confusion matrix: (pred\real)
	EN	PT	SP
EN	8402	7496	7147
PT	0	0	0
SP	0	0	0


On average:
Precision: 0
Accuracy: 0.6898676502495118
Recall: 0.0
F1: 0
\end{lstlisting}

\textbf{KLD model 2 (using 50 most frequent words)}
\begin{lstlisting}
Using limited amount of words
25360 samples

Confusion matrix: (pred\real)
	EN	PT	SP	
EN	0	0	0	
PT	9246	8251	7863	
SP	0	0	0	

On average: 
Precision: 0.1084516298633018
Accuracy: 0.5502365930599369
Recall: 0.3333333333333333
F1: 0.49097021808336555
\end{lstlisting}

\textbf{Distance model 1 (using all words)}
\begin{lstlisting}
25360 samples
Confusion matrix: (pred\real)
	EN	PT	SP	
EN	8404	7498	7147	
PT	842	753	715	
SP	0	0	1	

For label EN
Precision: 0.3646145168987809
Accuracy: 0.38931388012618295
Recall: 0.90893359290504
F1: 0.5204520823656913

For label PT
Precision: 0.32597402597402597
Accuracy: 0.642941640378549
Recall: 0.09126166525269665
F1: 0.14260013256320425

For label SP
Precision: 1.0
Accuracy: 0.6899842271293375
Recall: 0.00012717792191275595
F1: 0.000254323499491353

On average: 
Precision: 0.5635295142909357
Accuracy: 0.5740799158780231
Recall: 0.3334408120265498
F1: 0.6633065384283869
\end{lstlisting}

\textbf{Distance model 2 (using 50 most frequent words)}
\begin{multicols}{2}
\begin{lstlisting}
500 words in sample
27664 samples

Confusion matrix: (pred\real)
	EN	PT	SP	
EN	10055	7	0	
PT	30	8903	117	
SP	1	90	8461	

For label EN
Precision: 0.9993043132578017
Accuracy: 0.9986263736263736
Recall: 0.996926432678961
F1: 0.9981139567202699

For label PT
Precision: 0.9837569060773481
Accuracy: 0.9911798727588201
Recall: 0.9892222222222222
F1: 0.9864819944598338

For label SP
Precision: 0.9893592142188962
Accuracy: 0.9924812030075187
Recall: 0.9863604569829797
F1: 0.9878575598365441

On average: 
Precision: 0.9908068111846821
Accuracy: 0.9940958164642376
Recall: 0.9908363706280543
F1: 2.972453511016648
done

200 words in sample
39194 samples

Confusion matrix: (pred\real)
	EN	PT	SP	
EN	14250	7	0	
PT	39	12650	173	
SP	2	94	11979	

For label EN
Precision: 0.9995090131163639
Accuracy: 0.9987753227534827
Recall: 0.9971310615072423
F1: 0.9983186212694409

For label PT
Precision: 0.9835173378945732
Accuracy: 0.992014083788335
Recall: 0.9920790526233236
F1: 0.9877796431499629

For label SP
Precision: 0.9920496894409938
Accuracy: 0.9931367045976425
Recall: 0.9857636603028308
F1: 0.9888966855161596

On average: 
Precision: 0.991692013483977
Accuracy: 0.9946420370464867
Recall: 0.9916579248111322
F1: 2.9749949499355637
done

100 words in sample
62239 samples

Confusion matrix: (pred\real)
	EN	PT	SP	
EN	22627	14	0	
PT	63	20051	284	
SP	3	182	19015	

For label EN
Precision: 0.9993816527538536
Accuracy: 0.9987146323045036
Recall: 0.9970916141541445
F1: 0.9982353200688225

For label PT
Precision: 0.9829885282870869
Accuracy: 0.9912755667668183
Recall: 0.9903195535141008
F1: 0.9866404231762823

For label SP
Precision: 0.9903645833333333
Accuracy: 0.9924645318851524
Recall: 0.9852842116171823
F1: 0.9878178653991012

On average: 
Precision: 0.9909115881247579
Accuracy: 0.9941515769854914
Recall: 0.9908984597618092
F1: 2.9726936086442057
done

80 words in sample
91044 samples

Confusion matrix: (pred\real)
	EN	PT	SP	
EN	33098	34	1	
PT	90	27194	399	
SP	8	2388	27832	

For label EN
Precision: 0.998943651344581
Accuracy: 0.9985391678748737
Recall: 0.9970478370888058
F1: 0.9979948438842738

For label PT
Precision: 0.982335729509085
Accuracy: 0.968026448750055
Recall: 0.9182198811453268
F1: 0.9491963210527234

For label SP
Precision: 0.9207357416964403
Accuracy: 0.9692895742717806
Recall: 0.985831680362709
F1: 0.9521724255901471

On average: 
Precision: 0.9673383741833689
Accuracy: 0.9786183969655697
Recall: 0.9670331328656139
F1: 2.8993635905271447
done

70 words in sample
123964 samples

Confusion matrix: (pred\real)
	EN	PT	SP	
EN	45065	141	42	
PT	121	32474	493	
SP	13	7710	37905	

For label EN
Precision: 0.9959556223479491
Accuracy: 0.9974428059759285
Recall: 0.9970353326401027
F1: 0.9964951850254845

For label PT
Precision: 0.9814434235976789
Accuracy: 0.9317140460133587
Recall: 0.8053068815871047
F1: 0.8846934466647596

For label SP
Precision: 0.8307398965547471
Accuracy: 0.9333838856442193
Recall: 0.9860822060353798
F1: 0.9017699957177524

On average: 
Precision: 0.9360463141667917
Accuracy: 0.9541802458778355
Recall: 0.9294748067541958
F1: 2.7829586274079965
done

60 words in sample
162364 samples

Confusion matrix: (pred\real)
	EN	PT	SP	
EN	59024	925	81	
PT	159	35271	596	
SP	18	16618	49672	

For label EN
Precision: 0.9832417124770948
Accuracy: 0.9927139020965239
Recall: 0.9970101856387561
F1: 0.990078083719838

For label PT
Precision: 0.9790429134513963
Accuracy: 0.8873026040255229
Recall: 0.6678342863634642
F1: 0.7940342188203511

For label SP
Precision: 0.7491102129456476
Accuracy: 0.8933692197777833
Recall: 0.9865538540983932
F1: 0.8515905603607156

On average: 
Precision: 0.9037982796247129
Accuracy: 0.9244619086332767
Recall: 0.8837994420335379
F1: 2.6357028629009047
done

50 words in sample
208439 samples

Confusion matrix: (pred\real)
	EN	PT	SP	
EN	75821	3007	227	
PT	159	35380	596	
SP	20	29412	63817	

For label EN
Precision: 0.9590917715514515
Accuracy: 0.9836259049410139
Recall: 0.9976447368421053
F1: 0.9779884557092645

For label PT
Precision: 0.9791061297910613
Accuracy: 0.840845523150658
Recall: 0.5218366052596646
F1: 0.6808166721188447

For label SP
Precision: 0.684371950369441
Accuracy: 0.8548496202725977
Recall: 0.9872679455445544
F1: 0.8083780377353711

On average: 
Precision: 0.8741899505706513
Accuracy: 0.8931070161214233
Recall: 0.8355830958821081
F1: 2.46718316556348
done

20 words in sample
323611 samples

Confusion matrix: (pred\real)
	EN	PT	SP	
EN	75821	3007	227	
PT	159	35380	596	
SP	42014	66866	99541	

For label EN
Precision: 0.9590917715514515
Accuracy: 0.8596864754288328
Recall: 0.642583521195993
F1: 0.7695649305502692

For label PT
Precision: 0.9791061297910613
Accuracy: 0.781750311330579
Recall: 0.33614243774524244
F1: 0.5004668005771352

For label SP
Precision: 0.47759582767571407
Accuracy: 0.6610034887565627
Recall: 0.9917998485512733
F1: 0.6447269135482617

On average: 
Precision: 0.805264576339409
Accuracy: 0.7674800918386583
Recall: 0.6568419358308363
F1: 1.9147586446756661
\end{lstlisting}
\end{multicols}

\end{document}
